import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DigimonRequestComponent } from './pages/digimon-request/digimon-request.component';
import { DigimonRequestService } from './pages/digimon-request/services/digimon-request.service';

@NgModule({
    declarations: [AppComponent, DigimonRequestComponent],
    imports: [BrowserModule, AppRoutingModule, HttpClientModule],
    providers: [DigimonRequestService],
    bootstrap: [AppComponent],
})
export class AppModule {}
