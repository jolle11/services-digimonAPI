export interface DigimonInterface {
    name: string;
    img: string;
    level: string;
}
