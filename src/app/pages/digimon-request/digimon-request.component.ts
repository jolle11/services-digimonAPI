import { Component, OnInit } from '@angular/core';

import { DigimonInterface } from './models/idigimon.model';
import { DigimonRequestService } from './services/digimon-request.service';

@Component({
    selector: 'app-digimon-request',
    templateUrl: './digimon-request.component.html',
    styleUrls: ['./digimon-request.component.scss'],
})
export class DigimonRequestComponent implements OnInit {
    digimonList: DigimonInterface[] = [];

    constructor(private digimonRequestService: DigimonRequestService) {}

    ngOnInit(): void {
        this.digimonRequestService.getDigimons().subscribe((data) => {
            const digimonsArray = Object.values(data);
            const digimonList = digimonsArray.map((digimon) => {
                digimon = { name: digimon.name, img: digimon.img, level: digimon.level };
                return digimon;
            });
            this.digimonList = digimonList;
        });
    }
}
