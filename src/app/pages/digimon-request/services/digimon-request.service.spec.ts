import { TestBed } from '@angular/core/testing';

import { DigimonRequestService } from './digimon-request.service';

describe('DigimonRequestService', () => {
  let service: DigimonRequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DigimonRequestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
