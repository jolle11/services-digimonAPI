import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

const baseUrl = 'https://digimon-api.vercel.app/api/';

const digimonUrl = baseUrl + 'digimon';

@Injectable()
export class DigimonRequestService {
    constructor(private http: HttpClient) {}

    getDigimons() {
        return this.http.get(digimonUrl);
    }
}
