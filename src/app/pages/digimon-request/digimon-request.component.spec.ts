import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DigimonRequestComponent } from './digimon-request.component';

describe('DigimonRequestComponent', () => {
  let component: DigimonRequestComponent;
  let fixture: ComponentFixture<DigimonRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DigimonRequestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DigimonRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
